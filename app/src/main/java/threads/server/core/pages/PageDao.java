package threads.server.core.pages;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import threads.lite.cid.Cid;


@Dao
public interface PageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPage(Page page);

    @Query("SELECT * FROM Page WHERE id = :id")
    Page getPage(String id);

    @Query("UPDATE Page SET cid =:cid, sequence = :sequence WHERE id = :id")
    @TypeConverters(Cid.class)
    void setContent(String id, Cid cid, long sequence);

    @Query("UPDATE Page SET cid =:cid, sequence = sequence + 1  WHERE id = :id")
    @TypeConverters(Cid.class)
    void updateContent(String id, Cid cid);

    @Query("Select cid From Page WHERE id = :id")
    @TypeConverters(Cid.class)
    Cid getContent(String id);

}
