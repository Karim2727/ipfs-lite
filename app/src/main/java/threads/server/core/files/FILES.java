package threads.server.core.files;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import threads.lite.cid.Cid;

public class FILES {

    private static volatile FILES INSTANCE = null;

    private final FileInfoDatabase fileInfoDatabase;


    private FILES(FileInfoDatabase fileInfoDatabase) {
        this.fileInfoDatabase = fileInfoDatabase;
    }

    public static FILES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (FILES.class) {
                if (INSTANCE == null) {
                    FileInfoDatabase filesDatabase = Room.databaseBuilder(context,
                                    FileInfoDatabase.class,
                                    FileInfoDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().
                            build();
                    INSTANCE = new FILES(filesDatabase);
                }
            }
        }
        return INSTANCE;
    }


    public void clear() {
        getFileInfoDatabase().clearAllTables();
    }

    @NonNull
    public FileInfoDatabase getFileInfoDatabase() {
        return fileInfoDatabase;
    }

    public void setDeleting(long... idxs) {
        for (long idx : idxs) {
            getFileInfoDatabase().fileInfoDao().setDeleting(idx);
        }
    }

    public void resetDeleting(long... idxs) {
        for (long idx : idxs) {
            getFileInfoDatabase().fileInfoDao().resetDeleting(idx);
        }
    }

    public void setLeaching(long idx) {
        getFileInfoDatabase().fileInfoDao().setLeaching(idx);
    }

    public void resetLeaching(long idx) {
        getFileInfoDatabase().fileInfoDao().resetLeaching(idx);
    }

    public void setDone(long idx) {
        getFileInfoDatabase().fileInfoDao().setDone(idx);
    }

    public void setDone(long idx, @NonNull Cid cid) {
        getFileInfoDatabase().fileInfoDao().setDone(idx, cid);
    }

    public List<FileInfo> getAncestors(long idx) {
        List<FileInfo> path = new ArrayList<>();
        if (idx > 0) {
            FileInfo fileInfo = getFileInfo(idx);
            if (fileInfo != null) {
                path.addAll(getAncestors(fileInfo.getParent()));
                path.add(fileInfo);
            }
        }
        return path;
    }

    @NonNull
    public FileInfo createFileInfo(long parent) {
        return FileInfo.createFileInfo(parent);
    }

    private void delete(long idx) {
        getFileInfoDatabase().fileInfoDao().delete(idx);
    }

    public void finalDeleting(long... idxs) {
        for (long idx : idxs) {
            delete(idx);
        }
    }

    public long storeFileInfo(@NonNull FileInfo fileInfo) {
        return getFileInfoDatabase().fileInfoDao().insertFileInfo(fileInfo);
    }

    public void setName(long idx, @NonNull String name) {
        getFileInfoDatabase().fileInfoDao().setName(idx, name);
    }

    public void updateContent(long idx, @NonNull Cid cid, long size, long lastModified) {
        getFileInfoDatabase().fileInfoDao().updateContent(idx, cid, size, lastModified);
    }

    @Nullable
    public String getName(long idx) {
        return getFileInfoDatabase().fileInfoDao().getName(idx);
    }

    @NonNull
    public List<FileInfo> getPins() {
        return getFileInfoDatabase().fileInfoDao().getPins();
    }


    @NonNull
    public List<FileInfo> getChildren(long parent) {
        return getFileInfoDatabase().fileInfoDao().getChildren(parent);
    }

    @Nullable
    public FileInfo getFileInfo(long idx) {
        return getFileInfoDatabase().fileInfoDao().getFileInfo(idx);
    }

    @Nullable
    public Cid getContent(long idx) {
        return getFileInfoDatabase().fileInfoDao().getContent(idx);
    }

    public void setWork(long idx, @NonNull UUID id) {
        getFileInfoDatabase().fileInfoDao().setWork(idx, id.toString());
    }

    public void resetWork(long idx) {
        getFileInfoDatabase().fileInfoDao().resetWork(idx);
    }

    public void setUri(long idx, @NonNull String uri) {
        getFileInfoDatabase().fileInfoDao().setUri(idx, uri);
    }
}
