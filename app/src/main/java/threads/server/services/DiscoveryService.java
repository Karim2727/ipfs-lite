package threads.server.services;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import net.luminis.quic.Version;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.server.LogUtils;
import threads.server.core.DOCS;

public class DiscoveryService implements NsdManager.DiscoveryListener {
    private static final String TAG = DiscoveryService.class.getSimpleName();

    @NonNull
    private final IPFS ipfs;
    @NonNull
    private final DOCS docs;
    @NonNull
    private final NsdManager nsdManager;

    public DiscoveryService(@NonNull Context context, @NonNull NsdManager nsdManager) throws Exception {
        this.docs = DOCS.getInstance(context);
        this.ipfs = IPFS.getInstance(context);
        this.nsdManager = nsdManager;
    }

    @Override
    public void onStartDiscoveryFailed(String serviceType, int errorCode) {
        LogUtils.debug(TAG, "onStartDiscoveryFailed");
    }

    @Override
    public void onStopDiscoveryFailed(String serviceType, int errorCode) {
        LogUtils.debug(TAG, "onStopDiscoveryFailed");
    }

    @Override
    public void onDiscoveryStarted(String serviceType) {
        LogUtils.debug(TAG, "onDiscoveryStarted");
    }

    @Override
    public void onDiscoveryStopped(String serviceType) {
        LogUtils.debug(TAG, "onDiscoveryStopped");
    }


    @Override
    public void onServiceFound(NsdServiceInfo serviceInfo) {
        nsdManager.resolveService(serviceInfo, new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int i) {
                LogUtils.error(TAG, "onResolveFailed " + nsdServiceInfo.toString());
            }

            @Override
            public void onServiceResolved(NsdServiceInfo nsdServiceInfo) {
                try {
                    LogUtils.error(TAG, "onResolveResolved " + nsdServiceInfo.toString());
                    boolean connect = !Objects.equals(ipfs.self().toString(),
                            nsdServiceInfo.getServiceName());
                    if (connect) {
                        evaluate(nsdServiceInfo);
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        });
    }

    @Override
    public void onServiceLost(NsdServiceInfo serviceInfo) {
        LogUtils.error(TAG, "onServiceLost " + serviceInfo.toString());
        try {
            PeerId peerId = PeerId.decode(serviceInfo.getServiceName());
            docs.removeLocalAddress(peerId);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public void evaluate(@NonNull NsdServiceInfo serviceInfo) {
        try {
            PeerId decodedPeerId = PeerId.decode(serviceInfo.getServiceName());
            InetAddress inetAddress = serviceInfo.getHost();

            Multiaddr multiaddr = Multiaddr.create(Version.QUIC_version_1, decodedPeerId,
                    new InetSocketAddress(inetAddress, serviceInfo.getPort()));
            docs.addLocalAddress(multiaddr);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

}
