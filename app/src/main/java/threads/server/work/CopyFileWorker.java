package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.OutputStream;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.files.FILES;
import threads.server.core.files.FileInfo;

public class CopyFileWorker extends Worker {
    private static final String WID = "UFW";
    private static final String TAG = CopyFileWorker.class.getSimpleName();
    private final NotificationManager notificationManager;

    @SuppressWarnings("WeakerAccess")
    public CopyFileWorker(@NonNull Context context,
                          @NonNull WorkerParameters params) {
        super(context, params);
        notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    private static OneTimeWorkRequest getWork(@NonNull Uri uri, long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);
        data.putString(Content.URI, uri.toString());

        return new OneTimeWorkRequest.Builder(CopyFileWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void copyTo(@NonNull Context context, @NonNull Uri uri, long idx) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + idx, ExistingWorkPolicy.KEEP, getWork(uri, idx));

    }

    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);
        String uri = getInputData().getString(Content.URI);
        Objects.requireNonNull(uri);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());
            try (Session session = ipfs.createSession()) {

                FileInfo fileInfo = files.getFileInfo(idx);
                Objects.requireNonNull(fileInfo);

                Cid cid = fileInfo.getCid();
                Objects.requireNonNull(cid);

                Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                        InitApplication.STORAGE_CHANNEL_ID);

                builder.setContentText(fileInfo.getName())
                        .setSubText("" + 0 + "%")
                        .setProgress(100, 0, false)
                        .setOnlyAlertOnce(true)
                        .setSmallIcon(R.drawable.download)
                        .setCategory(Notification.CATEGORY_PROGRESS)
                        .setUsesChronometer(true)
                        .setOngoing(true);

                int notificationId = getId().hashCode();
                notificationManager.notify(notificationId, builder.build());

                try (OutputStream os = getApplicationContext().getContentResolver().
                        openOutputStream(Uri.parse(uri))) {
                    Objects.requireNonNull(os);
                    ipfs.fetchToOutputStream(session, os, cid, new Progress() {

                        @Override
                        public void setProgress(int progress) {
                            builder.setSubText("" + progress + "%")
                                    .setProgress(100, progress, false);
                            notificationManager.notify(notificationId, builder.build());
                        }

                        @Override
                        public boolean isCancelled() {
                            return isStopped();
                        }


                    });
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                } finally {
                    notificationManager.cancel(notificationId);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }
}
