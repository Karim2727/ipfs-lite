package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.pages.Page;
import threads.server.services.DaemonService;

public class PageWorker extends Worker {
    private static final String TAG = PageWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public PageWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork() {

        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        return new OneTimeWorkRequest.Builder(PageWorker.class)
                .addTag(TAG)
                .setConstraints(constraints)
                .setInitialDelay(5, TimeUnit.SECONDS)
                .build();

    }

    public static void publish(@NonNull Context context) {
        WorkManager.getInstance(context).
                enqueueUniqueWork(TAG, ExistingWorkPolicy.REPLACE, getWork());
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, "Start ...");

        try {

            if (DaemonService.publishEnabled()) {
                // only when the node is global reachable, publishing of the page make sense
                // it is assumed that with reservations (static or limited the node
                // is reachable (not always true, at least with limited)


                NotificationManager notificationManager = (NotificationManager)
                        getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                        InitApplication.PUBLISH_CHANNEL_ID);


                PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                        .createCancelPendingIntent(getId());
                String cancel = getApplicationContext().getString(android.R.string.cancel);

                Intent main = new Intent(getApplicationContext(), MainActivity.class);

                int requestID = (int) System.currentTimeMillis();
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                        main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                Notification.Action action = new Notification.Action.Builder(
                        Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                        intent).build();

                builder.setContentText(getApplicationContext().getString(R.string.publish_homepage))
                        .setContentIntent(pendingIntent)
                        .setOnlyAlertOnce(true)
                        .setSmallIcon(R.drawable.baseline_restore_24)
                        .addAction(action)
                        .setCategory(Notification.CATEGORY_SERVICE)
                        .setUsesChronometer(true)
                        .setOngoing(true);


                Notification notification = builder.build();

                int notificationId = getId().hashCode();

                notificationManager.notify(notificationId, notification);
                setForegroundAsync(new ForegroundInfo(notificationId, notification));

                publishPage();

            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, "Finish  onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }


    public void publishPage()
            throws Exception {

        DOCS docs = DOCS.getInstance(getApplicationContext());
        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        LogUtils.error(TAG, docs.getHomePageUri().toString());
        try (Session session = ipfs.createSession(false)) {

            int maxProviders = 100;
            int timeout = 60 * 5; // 5 min

            Page page = docs.getHomePage();
            if (page != null) {

                long seq = page.getSequence();
                Cid cid = page.getCid();
                Objects.requireNonNull(cid);
                LogUtils.error(TAG, "ipfs://" + cid);

                    try {
                        Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();
                        // publish ipns entry
                        ipfs.publishName(session, seq, cid, providers::add, new TimeoutCancellable(
                                () -> isStopped() || providers.size() > maxProviders,
                                timeout));

                        LogUtils.error(TAG, "Published ipns to " + providers.size());
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                    try {
                        Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();
                        // publish top-level content [!!! only !!!]
                        ipfs.provide(session, cid, providers::add, new TimeoutCancellable(
                                () -> isStopped() || providers.size() > maxProviders,
                                timeout));

                        LogUtils.error(TAG, "Published cid to " + providers.size());
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }


            }


        }
    }

}

