package threads.server.fragments;

public interface AccessFragment {
    void findInPage();

    void releaseActionMode();
}
