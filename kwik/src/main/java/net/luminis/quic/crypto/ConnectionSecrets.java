/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.crypto;


import net.luminis.quic.EncryptionLevel;
import net.luminis.quic.Role;
import net.luminis.quic.Version;
import net.luminis.quic.VersionHolder;
import net.luminis.hkdf.HKDF;
import net.luminis.tls.CipherSuite;
import net.luminis.tls.TrafficSecrets;

public class ConnectionSecrets {

    // https://tools.ietf.org/html/draft-ietf-quic-tls-29#section-5.2
    public static final byte[] STATIC_SALT_DRAFT_29 = new byte[]{
            (byte) 0xaf, (byte) 0xbf, (byte) 0xec, (byte) 0x28, (byte) 0x99, (byte) 0x93, (byte) 0xd2, (byte) 0x4c,
            (byte) 0x9e, (byte) 0x97, (byte) 0x86, (byte) 0xf1, (byte) 0x9c, (byte) 0x61, (byte) 0x11, (byte) 0xe0,
            (byte) 0x43, (byte) 0x90, (byte) 0xa8, (byte) 0x99};
    // https://www.rfc-editor.org/rfc/rfc9001.html#name-initial-secrets
    // "initial_salt = 0x38762cf7f55934b34d179ae6a4c80cadccbb7f0a"
    public static final byte[] STATIC_SALT_V1 = new byte[]{
            (byte) 0x38, (byte) 0x76, (byte) 0x2c, (byte) 0xf7, (byte) 0xf5, (byte) 0x59, (byte) 0x34, (byte) 0xb3,
            (byte) 0x4d, (byte) 0x17, (byte) 0x9a, (byte) 0xe6, (byte) 0xa4, (byte) 0xc8, (byte) 0x0c, (byte) 0xad,
            (byte) 0xcc, (byte) 0xbb, (byte) 0x7f, (byte) 0x0a};
    // https://www.ietf.org/archive/id/draft-ietf-quic-v2-01.html#name-initial-salt
    // "The salt used to derive Initial keys in Section 5.2 of [QUIC-TLS] changes to:
    //  initial_salt = 0xa707c203a59b47184a1d62ca570406ea7ae3e5d3"
    public static final byte[] STATIC_SALT_V2 = new byte[]{
            (byte) 0xa7, (byte) 0x07, (byte) 0xc2, (byte) 0x03, (byte) 0xa5, (byte) 0x9b, (byte) 0x47, (byte) 0x18,
            (byte) 0x4a, (byte) 0x1d, (byte) 0x62, (byte) 0xca, (byte) 0x57, (byte) 0x04, (byte) 0x06, (byte) 0xea,
            (byte) 0x7a, (byte) 0xe3, (byte) 0xe5, (byte) 0xd3};
    private final VersionHolder quicVersion;
    private final Role ownRole;
    private final Keys[] clientSecrets = new Keys[EncryptionLevel.values().length];
    private final Keys[] serverSecrets = new Keys[EncryptionLevel.values().length];
    private CipherSuite selectedCipherSuite;
    private byte[] originalDestinationConnectionId;


    public ConnectionSecrets(VersionHolder quicVersion, Role role) {
        this.quicVersion = quicVersion;
        this.ownRole = role;
    }

    /**
     * Generate the initial secrets
     */
    public synchronized void computeInitialKeys(byte[] destConnectionId) {
        this.originalDestinationConnectionId = destConnectionId;
        Version actualVersion = quicVersion.getVersion();

        byte[] initialSecret = computeInitialSecret(actualVersion);

        clientSecrets[EncryptionLevel.Initial.ordinal()] = new Keys(actualVersion, initialSecret, Role.Client);
        serverSecrets[EncryptionLevel.Initial.ordinal()] = new Keys(actualVersion, initialSecret, Role.Server);
    }

    /**
     * (Re)generates the keys for the initial peer secrets based on the given version. This is sometimes used during
     * version negotiation, when a packet with the "old" (original) version needs to be decoded.
     */
    public Keys getInitialPeerSecretsForVersion(Version version) {
        return new Keys(version, computeInitialSecret(version), ownRole.other());
    }

    private byte[] computeInitialSecret(Version actualVersion) {
        // https://www.rfc-editor.org/rfc/rfc9001.html#name-initial-secrets
        // "The hash function for HKDF when deriving initial secrets and keys is SHA-256"
        HKDF hkdf = HKDF.fromHmacSha256();

        byte[] initialSalt = actualVersion.isV1() ? STATIC_SALT_V1 : actualVersion.isV2() ? STATIC_SALT_V2 : STATIC_SALT_DRAFT_29;
        return hkdf.extract(initialSalt, originalDestinationConnectionId);
    }

    public void recomputeInitialKeys() {
        computeInitialKeys(originalDestinationConnectionId);
    }


    private void createKeys(EncryptionLevel level, CipherSuite selectedCipherSuite) {
        Keys clientHandshakeSecrets;
        Keys serverHandshakeSecrets;
        Version actualVersion = this.quicVersion.getVersion();

        if (selectedCipherSuite == CipherSuite.TLS_AES_128_GCM_SHA256) {
            clientHandshakeSecrets = new Keys(actualVersion, Role.Client);
            serverHandshakeSecrets = new Keys(actualVersion, Role.Server);
        } else {
            throw new IllegalStateException("unsupported cipher suite " + selectedCipherSuite);
        }
        clientSecrets[level.ordinal()] = clientHandshakeSecrets;
        serverSecrets[level.ordinal()] = serverHandshakeSecrets;

        // Keys for peer and keys for self must be able to signal each other of a key update.
        clientHandshakeSecrets.setPeerKeys(serverHandshakeSecrets);
        serverHandshakeSecrets.setPeerKeys(clientHandshakeSecrets);
    }

    public synchronized void computeHandshakeSecrets(TrafficSecrets secrets, CipherSuite selectedCipherSuite) {
        this.selectedCipherSuite = selectedCipherSuite;
        createKeys(EncryptionLevel.Handshake, selectedCipherSuite);

        clientSecrets[EncryptionLevel.Handshake.ordinal()].computeHandshakeKeys(secrets);
        serverSecrets[EncryptionLevel.Handshake.ordinal()].computeHandshakeKeys(secrets);
    }

    public synchronized void computeApplicationSecrets(TrafficSecrets secrets) {
        createKeys(EncryptionLevel.App, selectedCipherSuite);

        clientSecrets[EncryptionLevel.App.ordinal()].computeApplicationKeys(secrets);
        serverSecrets[EncryptionLevel.App.ordinal()].computeApplicationKeys(secrets);

    }

    public synchronized Keys getPeerSecrets(EncryptionLevel encryptionLevel) {
        return (ownRole == Role.Client) ? serverSecrets[encryptionLevel.ordinal()] :
                clientSecrets[encryptionLevel.ordinal()];
    }

    public synchronized Keys getOwnSecrets(EncryptionLevel encryptionLevel) {
        return (ownRole == Role.Client) ? clientSecrets[encryptionLevel.ordinal()] :
                serverSecrets[encryptionLevel.ordinal()];
    }


    public void discardHandshakeKeys() {
        clientSecrets[EncryptionLevel.Handshake.ordinal()] = null;
        serverSecrets[EncryptionLevel.Handshake.ordinal()] = null;
    }
}
