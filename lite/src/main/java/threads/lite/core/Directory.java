package threads.lite.core;

import threads.lite.cid.Cid;

public class Directory {
    private final Cid cid;
    private final long size;

    public Directory(Cid cid, long size) {
        this.cid = cid;
        this.size = size;
    }

    public Cid getCid() {
        return cid;
    }

    public long getSize() {
        return size;
    }
}
