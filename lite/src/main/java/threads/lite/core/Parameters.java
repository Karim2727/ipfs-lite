package threads.lite.core;

import net.luminis.quic.TransportParameters;

import threads.lite.IPFS;

public class Parameters extends TransportParameters {

    private boolean enableTrustManager = true;

    private Parameters(int maxIdleTimeoutInSeconds, int initialMaxStreamData,
                       int initialMaxStreamsBidirectional) {
        super(maxIdleTimeoutInSeconds, initialMaxStreamData,
                initialMaxStreamsBidirectional, 0);

    }

    public static Parameters getDefaultUniDirection(boolean enableTrustManager) {
        Parameters parameters = new Parameters(IPFS.GRACE_PERIOD, IPFS.MESSAGE_SIZE_MAX,
                0);
        parameters.setEnableTrustManager(enableTrustManager);
        return parameters;
    }

    public static Parameters getDefault() {
        return new Parameters(IPFS.GRACE_PERIOD, IPFS.MESSAGE_SIZE_MAX, IPFS.MAX_STREAMS);
    }

    public static Parameters getDefault(int maxIdleTimeoutInSeconds) {
        return new Parameters(maxIdleTimeoutInSeconds, IPFS.MESSAGE_SIZE_MAX, IPFS.MAX_STREAMS);
    }

    public boolean isEnableTrustManager() {
        return enableTrustManager;
    }

    public void setEnableTrustManager(boolean enableTrustManager) {
        this.enableTrustManager = enableTrustManager;
    }
}
