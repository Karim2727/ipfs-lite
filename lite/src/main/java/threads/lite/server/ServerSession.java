package threads.lite.server;

import androidx.annotation.NonNull;

import java.net.DatagramSocket;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

import bitswap.pb.MessageOuterClass;
import crypto.pb.Crypto;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.bitswap.BitSwapEngine;
import threads.lite.bitswap.BitSwapEngineHandler;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Keys;
import threads.lite.core.PeerStore;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Resolver;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteCertificate;
import threads.lite.host.LiteHost;
import threads.lite.host.LitePullHandler;
import threads.lite.host.LitePushHandler;
import threads.lite.host.LiteResponder;
import threads.lite.host.LiteServer;
import threads.lite.ident.IdentityHandler;
import threads.lite.utils.MultistreamHandler;


public final class ServerSession extends Session {

    private static final String TAG = ServerSession.class.getSimpleName();

    @NonNull
    private final BitSwapEngine bitSwapEngine;
    @NonNull
    private final Map<String, ProtocolHandler> protocols = new ConcurrentHashMap<>();
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    @NonNull
    private final LiteHost host;

    public ServerSession(@NonNull LiteHost host) {
        this.bitSwapEngine = new BitSwapEngine(host.getBlockStore());
        this.host = host;
        // add the default
        try {
            addProtocolHandler(new MultistreamHandler());
            addProtocolHandler(new LitePushHandler(host));
            addProtocolHandler(new LitePullHandler(host));
            addProtocolHandler(new IdentityHandler(this));
            addProtocolHandler(new BitSwapEngineHandler(bitSwapEngine));
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) throws Exception {

        if (isClosed()) throw new IllegalStateException("Session is closed");

        // some small tests
        String protocol = protocolHandler.getProtocol();
        Objects.requireNonNull(protocol);

        if (protocol.isEmpty()) {
            throw new Exception("invalid protocol name");
        }

        if (!protocol.startsWith("/")) {
            throw new Exception("invalid protocol name");
        }

        if (protocols.containsKey(protocol)) {
            throw new Exception("protocol name already exists");
        }

        protocols.put(protocol, protocolHandler);
    }

    @NonNull
    public Map<String, ProtocolHandler> getProtocols() {
        // important, protocols should note be modified "outside"
        return Collections.unmodifiableMap(protocols);
    }

    @NonNull
    @Override
    public LiteCertificate getLiteCertificate() {
        return host.getLiteCertificate();
    }

    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        throw new IllegalStateException("should not be invoked here");
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) {
        throw new IllegalStateException("should not be invoked here");
    }

    @Override
    public boolean isClosed() {
        return closed.get();
    }

    @Override
    public Set<Multiaddr> publishMultiaddrs() {
        return host.publishMultiaddrs(IPFS.MAX_PUBLISH_RESERVATIONS);
    }

    @Override
    public Crypto.PublicKey getPublicKey() {
        return host.getPublicKey();
    }


    @Override
    public Keys getKeys() {
        return host.getKeys();
    }

    @Override
    public LiteServer createServer(@NonNull DatagramSocket socket,
                                   @NonNull Consumer<Connection> connectConsumer,
                                   @NonNull Consumer<Connection> closedConsumer,
                                   @NonNull Function<PeerId, Boolean> isGated) {
        throw new IllegalStateException("should not be invoked here");
    }

    @NonNull
    @Override
    public BlockStore getBlockStore() {
        return host.getBlockStore();
    }

    @NonNull
    @Override
    public PeerStore getPeerStore() {
        return host.getPeerStore();
    }

    @Override
    public boolean isFindProvidersActive() {
        return false;
    }

    @Override
    public PeerId self() {
        return host.self();
    }

    @NonNull
    @Override
    public StreamHandler getStreamHandler() {
        return new LiteResponder(protocols);
    }

    @NonNull
    @Override
    public Resolver getResolver() {
        return host.getResolver();
    }


    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] key, @NonNull byte[] data) {
        throw new IllegalStateException("should not be invoked here");
    }

    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId peerId) {
        throw new IllegalStateException("should not be invoked here");
    }

    @Override
    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer, @NonNull byte[] key) {
        throw new IllegalStateException("should not be invoked here");
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> multiaddrConsumer,
                              @NonNull Consumer<PeerId> peerIdConsumer,
                              @NonNull Cid cid) {
        throw new IllegalStateException("should not be invoked here");
    }

    @Override
    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new IllegalStateException("should not be invoked here");
    }

    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId) {
        throw new IllegalStateException("should not be invoked here");
    }

    @NonNull
    @Override
    public Set<Peer> getRoutingPeers() {
        return host.getRoutingPeers();
    }

    @Override
    public void close() {
        try {
            bitSwapEngine.close();
            swarm.clear(); // connections are closed in bitswap anyway
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            closed.set(true);
        }
    }
}

