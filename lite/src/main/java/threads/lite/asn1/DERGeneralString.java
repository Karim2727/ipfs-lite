package threads.lite.asn1;

/**
 * ASN.1 GENERAL-STRING data type.
 * <p>
 * This is an 8-bit encoded ISO 646 (ASCII) character set
 * with optional escapes to other character sets.
 * </p>
 */
public class DERGeneralString extends ASN1GeneralString {
    DERGeneralString(byte[] contents) {
        super(contents);
    }
}
