package threads.lite.relay;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import circuit.pb.Circuit;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Reservation;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.holepunch.HolePunch;
import threads.lite.host.LiteStream;
import threads.lite.ident.IdentityService;
import threads.lite.mplex.MuxedStream;
import threads.lite.mplex.MuxedTransport;
import threads.lite.noise.CipherStatePair;
import threads.lite.noise.Handshake;
import threads.lite.noise.Noise;
import threads.lite.utils.DataHandler;

public final class RelayHandler implements StreamHandler {
    private static final String TAG = RelayHandler.class.getSimpleName();

    private final RelayConnection relayConnection;

    private Noise.NoiseState initiator;


    public RelayHandler(RelayConnection relayConnection) {
        this.relayConnection = relayConnection;
    }


    private Noise.NoiseState getInitiator() throws Exception {
        if (initiator == null) {
            initiator = Noise.getInitiator(
                    relayConnection.getPeerId(), relayConnection.getSession().getKeys());
        }
        return initiator;
    }

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        if (relayConnection.initMode()) {
            LogUtils.error(TAG, "Init Mode " + throwable.getMessage());
            stream.close();
            relayConnection.throwable(throwable);
        } else {
            if (stream.isInitiator()) {
                relayConnection.getStreamHandler(stream.getStreamId()).throwable(stream, throwable);
            } else {
                LogUtils.error(TAG, throwable.getMessage());
                stream.close();
            }
        }
    }

    @Override
    public void protocol(Stream stream, String protocol) throws Exception {

        LogUtils.warning(TAG, "Protocol " + protocol +
                " streamId " + stream.getStreamId() + " initiator " + stream.isInitiator());

        stream.setAttribute(PROTOCOL, protocol);

        if (!relayConnection.initMode()) {
            if (!stream.isInitiator()) {
                ProtocolHandler protocolHandler =
                        relayConnection.getSession().getProtocolHandler(protocol);
                if (protocolHandler != null) {
                    protocolHandler.protocol(stream);
                } else {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA));
                }
            } else {
                relayConnection.getStreamHandler(stream.getStreamId())
                        .protocol(stream, protocol);
            }
        } else {
            switch (protocol) {
                case IPFS.MULTISTREAM_PROTOCOL:
                    if (!stream.isInitiator()) {
                        stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL));
                    }
                    break;
                case IPFS.RELAY_PROTOCOL_HOP: {
                    if (!stream.isInitiator()) {
                        throw new Exception("wrong initiator " + protocol);
                    } else {
                        Circuit.Peer dest = Circuit.Peer.newBuilder()
                                .setId(ByteString.copyFrom(relayConnection.getPeerId().encoded()))
                                .build();

                        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                                .setType(Circuit.HopMessage.Type.CONNECT)
                                .setPeer(dest)
                                .build();
                        stream.writeOutput(DataHandler.encode(message));
                    }
                    break;
                }
                case IPFS.IDENTITY_PROTOCOL:
                    if (!stream.isInitiator()) {

                        Set<String> protocols = new HashSet<>();
                        protocols.add(IPFS.MULTISTREAM_PROTOCOL);
                        protocols.add(IPFS.MPLEX_PROTOCOL);
                        protocols.add(IPFS.RELAY_PROTOCOL_HOP);
                        protocols.add(IPFS.IDENTITY_PROTOCOL);
                        protocols.add(IPFS.NOISE_PROTOCOL);
                        protocols.add(IPFS.HOLE_PUNCH_PROTOCOL);

                        // only when a static relay connection is available
                        if (relayConnection.isStaticRelay()) {
                            protocols.addAll(relayConnection.getSession().getProtocolNames());
                        }

                        Session session = relayConnection.getSession();
                        IdentifyOuterClass.Identify response = IdentityService.createIdentity(
                                session.getPublicKey(), protocols, session.publishMultiaddrs(),
                                stream.getConnection().remoteMultiaddr());

                        stream.writeOutput(DataHandler.encodeProtocols(IPFS.IDENTITY_PROTOCOL));
                        // Note: the closeOutput is done on the muxed stream (does not close
                        // the underlying stream
                        stream.writeOutput(DataHandler.encode(response)).thenApply(Stream::closeOutput);
                    } else {
                        relayConnection.getStreamHandler(stream.getStreamId()).protocol(stream, protocol);
                    }
                    break;
                case IPFS.NOISE_PROTOCOL:
                    if (!stream.isInitiator()) {
                        throw new Exception("wrong initiator " + protocol);
                    } else {

                        stream.writeOutput(Noise.encodeNoiseMessage(getInitiator().getInitalMessage()));

                        LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);
                        stream.setAttribute(TRANSPORT, new Handshake(liteStream.getQuicStream()));
                        LogUtils.error(TAG, "Transport set to Handshake");
                    }
                    break;
                case IPFS.HOLE_PUNCH_PROTOCOL:
                    if (!stream.isInitiator()) {
                        stream.writeOutput(DataHandler.encodeProtocols(IPFS.HOLE_PUNCH_PROTOCOL));
                    } else {
                        throw new Exception("wrong initiator " + protocol);
                    }
                    break;
                default:
                    LogUtils.error(TAG, "Ignore " + protocol);
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA));
            }
        }
    }


    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {

        String protocol = (String) stream.getAttribute(PROTOCOL);
        LogUtils.warning(TAG, "data streamId " + stream.getStreamId() +
                " protocol " + protocol + " initiator " + stream.isInitiator());
        Objects.requireNonNull(protocol);

        if (!relayConnection.initMode()) {
            if (!stream.isInitiator()) {
                ProtocolHandler protocolHandler = null;
                if (relayConnection.isStaticRelay()) {
                    protocolHandler = relayConnection.getSession().getProtocolHandler(protocol);
                }
                if (protocolHandler != null) {
                    protocolHandler.protocol(stream);
                } else {
                    throw new Exception("Protocol " + protocol + " not supported data " + data);
                }
            } else {
                relayConnection.getStreamHandler(stream.getStreamId()).data(stream, data);
            }
        } else {

            switch (protocol) {
                case IPFS.NOISE_PROTOCOL: {

                    if (!stream.isInitiator()) {
                        throw new Exception("wrong initiator " + protocol);
                    } else {
                        Noise.Response response = getInitiator().handshake(data.array());

                        byte[] msg = response.getMessage();
                        if (msg != null) {
                            stream.writeOutput(Noise.encodeNoiseMessage(msg));
                        }


                        CipherStatePair cipherStatePair = response.getCipherStatePair();

                        if (cipherStatePair != null) {

                            if (stream instanceof MuxedStream) {
                                throw new Exception("not excepted stream");
                            }
                            LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);


                            // upgrade connection
                            MuxedTransport muxedTransport = new MuxedTransport(
                                    liteStream.getQuicStream(),
                                    cipherStatePair.getSender(),
                                    cipherStatePair.getReceiver());
                            stream.setAttribute(TRANSPORT, muxedTransport);

                            LogUtils.error(TAG, "Transport set to MuxedTransport");

                            // notify relay connection
                            relayConnection.upgradeTransport(muxedTransport);

                        }
                    }
                    break;
                }
                case IPFS.RELAY_PROTOCOL_HOP: {

                    if (!stream.isInitiator()) {
                        throw new Exception("wrong initiator " + protocol);
                    } else {
                        Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data.array());
                        Objects.requireNonNull(msg);
                        if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                            throw new Exception(msg.getType().name());
                        }

                        if (msg.getStatus() != Circuit.Status.OK) {
                            throw new Exception(msg.getStatus().name());
                        }

                        Reservation.Limit limit = RelayService.getReservationLimit(msg);
                        relayConnection.setLimit(limit);

                        stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL,
                                IPFS.NOISE_PROTOCOL));
                    }
                    break;
                }
                case IPFS.HOLE_PUNCH_PROTOCOL:
                    if (!stream.isInitiator()) {
                        HolePunch.response(relayConnection, stream, data);
                    } else {
                        throw new Exception("wrong initiator " + protocol);
                    }
                    break;
                default:
                    throw new Exception("Protocol " + protocol + " not supported data " + data);
            }
        }

    }

    @Override
    public void streamTerminated(QuicStream quicStream) {
        LogUtils.error(TAG, "stream terminated, here we can close the connection");
        quicStream.getConnection().close();
    }

}
