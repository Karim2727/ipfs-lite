package threads.lite.mplex;

import android.util.SparseArray;


public enum MplexFlag {

    NewStream(0),
    MessageReceiver(1),
    MessageInitiator(2),
    CloseReceiver(3),
    CloseInitiator(4),
    ResetReceiver(5),
    ResetInitiator(6);

    private static final SparseArray<MplexFlag> byValue = new SparseArray<>();

    static {
        for (MplexFlag t : MplexFlag.values()) {
            byValue.put(t.getValue(), t);
        }
    }

    private final int value;

    MplexFlag(int value) {
        this.value = value;
    }

    public static MplexFlag get(int value) {
        MplexFlag protocol = byValue.get(value);
        if (protocol == null) {
            throw new IllegalStateException("No mplex flag with code: " + value);
        }
        return protocol;
    }

    public static boolean isInitiator(MplexFlag mplexFlag) {
        return mplexFlag.getValue() % 2 == 0;
    }

    public static MuxFlag toAbstractFlag(MplexFlag mplexFlag) {
        switch (mplexFlag) {
            case NewStream:
                return MuxFlag.OPEN;
            case MessageReceiver:
            case MessageInitiator:
                return MuxFlag.DATA;
            case CloseReceiver:
            case CloseInitiator:
                return MuxFlag.CLOSE;
            case ResetReceiver:
            case ResetInitiator:
                return MuxFlag.RESET;
            default:
                throw new IllegalStateException("Unknown mplex flag");
        }
    }

    public static MplexFlag toMplexFlag(MuxFlag muxFlag, boolean initiator) {
        switch (muxFlag) {
            case OPEN:
                return NewStream;
            case RESET:
                if (initiator)
                    return ResetInitiator;
                else
                    return ResetReceiver;
            case CLOSE:
                if (initiator)
                    return CloseInitiator;
                else
                    return CloseReceiver;
            case DATA:
                if (initiator)
                    return MessageInitiator;
                else
                    return MessageReceiver;
            default:
                throw new IllegalStateException("Unknown mux flag");
        }
    }

    public int getValue() {
        return value;
    }
}
