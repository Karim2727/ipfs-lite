package threads.lite.peerstore;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

@Dao
public interface PeerStoreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMultiaddr(Multiaddr multiaddr);

    @Query("SELECT * FROM Multiaddr WHERE peerId = :peerId")
    Multiaddr getMultiaddr(PeerId peerId);

    @Delete
    void removeMultiaddr(Multiaddr multiaddr);
}
