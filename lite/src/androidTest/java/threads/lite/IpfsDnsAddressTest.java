package threads.lite;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;

import threads.lite.cid.Multiaddr;

@RunWith(AndroidJUnit4.class)
public class IpfsDnsAddressTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_dnsAddress() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        Multiaddr dnsaddr = Multiaddr.create(
                "/dnsaddr/bootstrap.libp2p.io/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");
        Set<Multiaddr> addresses = ipfs.resolveDnsaddr(dnsaddr);
        assertNotNull(addresses);
        assertFalse(addresses.isEmpty());
    }

    @Test
    public void test_dnsAddress2() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        Multiaddr dnsaddr = Multiaddr.create(
                "/dnsaddr/ipfs.jamonbread.tech/p2p/12D3KooWHnwgjLJjDgBVBxFJXGJYzrwM9RrsYi9KqJYDNoaGYBhC");
        Set<Multiaddr> addresses = ipfs.resolveDnsaddr(dnsaddr);
        assertNotNull(addresses);
        assertTrue(addresses.isEmpty());
    }

    @Test
    public void test_dns() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        // not working example (reason might be that the "quicweb3-storage-am6.web3.dwebops.net"
        // is not known by google dns resolver)
        // must use the dns server which was evaluated by dnsaddr
        // -> but still good for code coverage
        Multiaddr dnsaddr = Multiaddr.create(
                "/dns4/quicweb3-storage-am6.web3.dwebops.net/udp/4001/quic/p2p/12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW");
        Multiaddr multiaddr = ipfs.resolveDns(dnsaddr);
        assertNull(multiaddr);


        dnsaddr = Multiaddr.create("/dns4/luflosi.de/udp/4002/quic/p2p/12D3KooWBqQrnTqx9Wp89p2bD1hrwmXYJQ5x1fDfigRCfZJGKQfr/p2p-circuit/p2p/12D3KooWKQTnJ3vkV3aQFKiZQKJTfqrzj75Gfpp6CLMdBvqpu6Ac");
        multiaddr = ipfs.resolveDns(dnsaddr);
        assertNotNull(multiaddr);

    }
}
