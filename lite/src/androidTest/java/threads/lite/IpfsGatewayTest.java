package threads.lite;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsGatewayTest {

    private static final String TAG = IpfsGatewayTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_gateway() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {

            String text = "Hallo das ist ein Test um den Gateway";
            Cid cid = ipfs.storeText(session, text);
            assertNotNull(cid);

            Set<Multiaddr> providers = new HashSet<>();

            long start = System.currentTimeMillis();

            ipfs.provide(session, cid, providers::add, new TimeoutCancellable(120));


            LogUtils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());


            // Now lets check if the gateway can find the cid (and makes a connection to ourself)

            URL url = new URL("https://ipfs.io/ipfs/" + cid);
            LogUtils.error(TAG, url.toString());

            File file = TestEnv.createCacheFile(context);

            IPFS.downloadUrl(url, file, 120);//120 sec
            assertTrue(file.length() > 0);
            assertEquals(file.length(), text.length());

        }
    }

}